# http://gl.developpez.com/tutoriel/outil/makefile/
# https://www.gnu.org/software/make/manual/html_node/File-Name-Functions.html

DEBUG ?= no
# DEBUG = yes
export DEBUG

##########################################
# param
##########################################
make = make --makefile=./Makefile_targeted_programm.make

ifeq ($(DEBUG),no)
	make += --jobs
endif

##########################################
# redirect all rules
##########################################
all: compile documentation

documentation:
	@plantuml -tsvg -nbthread auto **.plantuml

ifdef TARGET
%:
	@$(make) $@ TARGET=$(TARGET)
else
%:
	@$(make) $@ TARGET=daemon
	@$(make) $@ TARGET=client
endif
