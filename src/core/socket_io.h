#ifndef SOCKET_IO_H
	#define SOCKET_IO_H
	
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <unistd.h>
	
	#include "const.h"
	
	int socket_read_and_check(const int command_fd, const char* should_be_in_socket);
	int socket_write(const int command_fd, const char* data);
#endif
