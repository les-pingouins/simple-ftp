#include "string_util.h"

char* strstrip(const char* str) {
	size_t size = strlen(str);
	
	if (!size) {
		return str;
	}
	
	// right trim
	char* end = str + size - 1;
	while (end >= str && isspace(*end)) {
		end--;
	}
	*(end + 1) = '\0';
	
	// left trim
	while (*str && isspace(*str)) {
		str++;
	}
	
	return str;
}


int startsWith(const char* str, const char* pre) {
	size_t lenstr = strlen(str);
	size_t lenpre = strlen(pre);
	return lenstr < lenpre ? 0 : strncmp(str, pre, lenpre) == 0;
}
