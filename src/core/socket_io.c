#include "socket_io.h"

int socket_read_and_check(const int command_fd, const char* should_be_in_socket) {
	char socket_data[BUFFER_SIZE] = "";
	int read_length = read(command_fd, socket_data, BUFFER_SIZE);
	
	if (!strcmp(socket_data, should_be_in_socket)) {
		return EXIT_SUCCESS;
	}
	
	if (strlen(socket_data) == 0) {
		fprintf(stderr, "No data readed in the socket!\n");
	}
	else {
		fprintf(stderr, "Readed data in the socket: “%s” should be “%s”!\n", socket_data, should_be_in_socket);
	}
	
	return EXIT_FAILURE;
}


int socket_write(const int command_fd, const char* data) {
	int to_write = strlen(data) + 1;
	
	if (write(command_fd, data, to_write) != to_write) {
		fprintf(stderr, "Can't write in the socket!\n");
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}
