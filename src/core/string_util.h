#ifndef STRING_UTIL_H
	#define STRING_UTIL_H
	
	#include <ctype.h>
	#include <string.h>
	
	char* strstrip(const char* str);
	int startsWith(const char* str, const char* pre);
#endif
