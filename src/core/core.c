#include "core.h"

void addr_info(const struct sockaddr_in addr, char* address, int* port) {
	inet_ntop(AF_INET, &addr.sin_addr.s_addr, address, 255);
	*port = ntohs(addr.sin_port);
}


int exec_sys_cmd(const int command_fd, const char* cmd) {
	pid_t pid = fork();
	
	if (pid == 0) {
		if (dup2(command_fd, STDOUT_FILENO) == -1) {
			perror("Can't duplicate output stream to command file descriptor");
			return EXIT_FAILURE;
		}
		
		char* cmd_copy = calloc(strlen(cmd) + 1, sizeof(char));
		strcpy(cmd_copy, cmd);
		char* part = strtok(cmd_copy, " ");
		char* exe;
		char* arg[255];
		int i = 0;
		
		exe = part;
		
		if ('/' != part[0]) {
			arg[i++] = part;
		}
		
		while (i < 254 && (part = strtok(NULL, " ")) != NULL) {
			arg[i++] = part;
		}
		arg[i] = NULL;
		
		execvp(exe, arg);
		
		perror("Exec system command");
		
		// if we are here, the exec hasn't replace this program while executing
		return EXIT_FAILURE;
	}
	else if (pid > 0) {
		int status;
		
		// wait for sub command finish
		wait(&status);
		
		if (WIFEXITED(status)) {
			status = WEXITSTATUS(status);
			
			if (status) {
				fprintf(stderr, "Exec system command exited with status: %i\n", status);
			}
		}
		else {
			status = WTERMSIG(status);
			
			if (status) {
				fprintf(stderr, "Exec system command stoped by: [%i] %s\n", status, strsignal(status));
			}
		}
		
		if (status) {
			return status;
		}
	}
	else {
		perror("Exec fork");
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}
