#ifndef SOCKET_MANAGER_H
	#define SOCKET_MANAGER_H
	
	#include <stdio.h>
	#include <stdlib.h>
	#include <unistd.h>
	#include <string.h>
	#include <signal.h>
	#include <arpa/inet.h>
	#include <netinet/in.h>
	#include <sys/socket.h>
	
	#include "const.h"
	#include "string_util.h"
	
	#define MIN_PORT 49152
	#define MAX_PORT 65535

	int server_init(const int port, const int max_connection_allowed);
	int server_random_port_init(int* port, const int max_connection_allowed);
	int connect_to_server(const char* addr, const int port);
#endif
