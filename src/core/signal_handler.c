#include "signal_handler.h"

void stop_process_all(int sig) {
	// prevents signal “loop”
	signal(SIGTERM, stop_process_listener);
	
	// kill current process and all of his children
	pid_t pid = getpid();
	kill(pid * -1, SIGINT);
}


void stop_process_listener(int sig) {
	printf("Stop server [%i]\n", getpid());
	exit(EXIT_SUCCESS);
}


void stop_process_connection(int sig) {
	printf("Stop connection [%i]\n", getpid());
	exit(EXIT_FAILURE);
}
