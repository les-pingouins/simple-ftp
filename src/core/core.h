#ifndef CORE_H
	#define CORE_H
	
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <unistd.h>
	#include <signal.h>
	#include <netinet/in.h>
	#include <sys/socket.h>
	#include <sys/wait.h>
	
	#include "const.h"
	#include "socket_manager.h"
	#include "socket_io.h"
	#include "string_util.h"
	#include "socket_manager.h"
	#include "socket_io.h"
	#include "file_transfert.h"
	#include "signal_handler.h"
	
	void help_program();
	
	void addr_info(const struct sockaddr_in addr, char* address, int* port);
	int exec_sys_cmd(const int command_fd, const char* cmd);
#endif
