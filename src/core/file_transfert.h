#ifndef FILE_TRANSFERT_H
	#define FILE_TRANSFERT_H
	
	#include <stdio.h>
	#include <stdlib.h>
	#include <unistd.h>
	#include <pthread.h>
	#include <signal.h>
	#include <sys/socket.h>
	
	#include "const.h"
	#include "socket_manager.h"
	#include "socket_io.h"
	
	struct _transfert_thread_arg {
		const int listener_fd;
		char address[255];
		const int port;
		char path[255];
	};
	
	int copy_file(FILE* input, FILE* output);
	int socket_to_file(const int transfert_fd, const char* path);
	int file_to_socket(const int transfert_fd, const char* path);
	int send_file(const int command_fd, const char* address, const char* path);
	int receive_file(const int command_fd, const char* address, const char* path);
	void _send_file_thread(void* parg);
	void _receive_file_thread(void* parg);
#endif
