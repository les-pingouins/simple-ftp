#include "socket_manager.h"

int server_init(const int port, const int max_connection_allowed) {
	const int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_fd == -1) {
		return -1;
	}
	
	struct sockaddr_in server_addr;
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	server_addr.sin_port = htons(port);
	if (bind(socket_fd, (struct sockaddr *)&server_addr, sizeof(server_addr))) {
		return -1;
	}
	
	if (listen(socket_fd, max_connection_allowed)) {
		return -1;
	}
	
	return socket_fd;
}


int server_random_port_init(int* port, const int max_connection_allowed) {
	*port = MIN_PORT;
	int socket_fd = -1;
	while ((socket_fd = server_init(*port, max_connection_allowed)) == -1) {
		if (*port == MAX_PORT) {
			perror("No port available, can't download file");
			return -1;
		}
		(*port)++;
	}
	return socket_fd;
}


int connect_to_server(const char* addr, const int port) {
	const int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_fd == -1) {
		perror("Socket");
		return 0;
	}
	
	struct sockaddr_in server_addr;
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	if (!inet_pton(AF_INET, addr, &server_addr.sin_addr.s_addr)) {
		perror("Can't convert IP server to number");
	}
	server_addr.sin_port = htons(port);
	if (connect(socket_fd, (struct sockaddr *)&server_addr, sizeof(server_addr))) {
		fprintf(stderr, "Server: %s:%i\n", addr, port);
		perror("Connect");
		return 0;
	}
	
	return socket_fd;
}
