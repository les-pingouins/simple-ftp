#include "file_transfert.h"

int copy_file(FILE* input, FILE* output) {
	if (!input || !output) {
		perror("Error");
		return EXIT_FAILURE;
	}
	
	char buffer[BUFFER_SIZE] = "";
	int readed = 0;
	while ((readed = fread(buffer, sizeof(*buffer), BUFFER_SIZE, input))) {
		if (readed != fwrite(buffer, sizeof(*buffer), BUFFER_SIZE, output)) {
			perror("Can't write to buffer");
			return EXIT_FAILURE;
		}
	}
	
	fclose(input);
	free(output);
	
	return EXIT_SUCCESS;
}


int socket_to_file(const int transfert_fd, const char* path) {
	FILE* output_file = fopen(path, "w");
	FILE* command_file = fdopen(transfert_fd, "r");
	
	if (!output_file) {
		printf("Can't open file “%s” in write mode\n", path);
	}
	if (!command_file) {
		printf("Can't open file descriptor as FILE “%i” in read mode\n", transfert_fd);
	}
	
	return copy_file(command_file, output_file);
}


int file_to_socket(const int transfert_fd, const char* path) {
	FILE* input_file = fopen(path, "r");
	FILE* command_file = fdopen(transfert_fd, "w");
	
	if (!input_file) {
		printf("Can't open file “%s” in read mode\n", path);
	}
	if (!command_file) {
		printf("Can't open file descriptor as FILE “%i” in write mode\n", transfert_fd);
	}
	
	return copy_file(input_file, command_file);
}


int send_file(const int command_fd, const char* address, const char* path) {
	int port = 0;
	const int listener_fd = server_random_port_init(&port, 1);
	if (listener_fd == -1) {
		perror("No port available, can't send file");
		return EXIT_FAILURE;
	}
	
	printf("Listenning on %i for %s for transfert the file “%s”\n", port, address, path);
	
	alarm(TIMEOUT_DELAY);
	char socket_data[12];
	sprintf(socket_data, "port %i", port);
	if (socket_write(command_fd, socket_data)) {
		perror("Can't write to the socket");
		return EXIT_FAILURE;
	}
	alarm(0);
	
	// sub process way
	// signal(SIGCHLD, SIG_IGN);
	// pid_t pid = fork();
	// if (pid == 0) {
	// 	// struct sockaddr_in client_addr;
	// 	// socklen_t clilen = sizeof(clilen);
	// 	// int transfert_fd = accept(listener_fd, &client_addr, &clilen);
	// 	int transfert_fd = accept(listener_fd, NULL, NULL);
	//
	// 	if (socket_to_file(transfert_fd, path)) {
	// 		perror("Can't send the file");
	// 		return EXIT_FAILURE;
	// 	}
	//
	// 	close(transfert_fd);
	//
	// 	printf("Transfert done ! Sent to %s for the file “%s”\n", address, path);
	// }
	// signal(SIGCHLD, SIG_DFL);
	
	// thread way
	struct _transfert_thread_arg arg = {
		.listener_fd = listener_fd,
		.port = port
	};
	strcpy(arg.address, address);
	strcpy(arg.path, path);
	
	pthread_t tid;
	pthread_attr_t attr;
	if (pthread_attr_init(&attr)
	 || pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED)
	 || pthread_create(&tid, &attr, (void * (*)(void *))_send_file_thread, (void*)&arg)) {
		perror("Can't transfert file file");
		return EXIT_FAILURE;
	}
	pthread_attr_destroy(&attr);
	
	return EXIT_SUCCESS;
}

int receive_file(const int command_fd, const char* address, const char* path) {
	alarm(TIMEOUT_DELAY);
	char socket_data[12] = "";
	if (!read(command_fd, socket_data, 12) || !startsWith(socket_data, "port ")) {
		perror("Can't read the port to transfert the file");
		return EXIT_FAILURE;
	}
	alarm(0);
	
	int port = atoi(socket_data + 5);
	
	// sub process way
	// signal(SIGCHLD, SIG_IGN);
	// pid_t pid = fork();
	// if (pid == 0) {
	// 	int upload_fd = connect_to_server(address, port);
	// 	file_to_socket(upload_fd, path);
	// 	close(upload_fd);
	//
	// 	printf("Transfert done ! Received from %s:%i for the file “%s”\n", address, port, path);
	// }
	// signal(SIGCHLD, SIG_DFL);
	
	// thread way
	struct _transfert_thread_arg arg = { .port = port };
	strcpy(arg.address, address);
	strcpy(arg.path, path);
	
	pthread_t tid;
	pthread_attr_t attr;
	if (pthread_attr_init(&attr)
	 || pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED)
	 || pthread_create(&tid, &attr, (void*(*)(void *))_receive_file_thread, (void*)&arg)) {
		perror("Can't transfert file file");
		return EXIT_FAILURE;
	}
	pthread_attr_destroy(&attr);
	
	return EXIT_SUCCESS;
}


void _send_file_thread(void* parg) {
	struct _transfert_thread_arg arg = *(struct _transfert_thread_arg*) parg;
	
	// struct sockaddr_in client_addr;
	// socklen_t clilen = sizeof(clilen);
	// int transfert_fd = accept(arg.listener_fd, &client_addr, &clilen);
	int transfert_fd = accept(arg.listener_fd, NULL, NULL);
	if (transfert_fd == -1) {
		perror("Can't get the connection");
		return;
	}
	close(arg.listener_fd);
	
	if (file_to_socket(transfert_fd, arg.path)) {
		perror("Can't send the file");
		return;
	}
	
	close(transfert_fd);
	
	printf("Transfert done ! Sent to %s for the file “%s”\n", arg.address, arg.path);
}


void _receive_file_thread(void* parg) {
	struct _transfert_thread_arg arg = *(struct _transfert_thread_arg*) parg;
	
	int transfert_fd = connect_to_server(arg.address, arg.port);
	if (transfert_fd == -1) {
		perror("Can't connect");
		return;
	}
	
	if (socket_to_file(transfert_fd, arg.path)) {
		perror("Can't receive the file");
		return;
	}
	close(transfert_fd);
	
	printf("Transfert done ! Received from %s:%i for the file “%s”\n", arg.address, arg.port, arg.path);
}
