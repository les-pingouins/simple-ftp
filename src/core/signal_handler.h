#ifndef SIGNAL_HANDLER_H
	#define SIGNAL_HANDLER_H
	
	#include <stdio.h>
	#include <stdlib.h>
	#include <signal.h>
	#include <unistd.h>
	
	void stop_process_all(int sig);
	void stop_process_listener(int sig);
	void stop_process_connection(int sig);
#endif
