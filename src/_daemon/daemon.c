#include "daemon.h"

int main(int argc, char const *argv[]) {
	int port = COMMAND_PORT;
	
	if (argc >= 2) {
		if (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help")) {
			help_program();
			return EXIT_SUCCESS;
		}
		
		port = atoi(argv[1]);
	}
	
	// CTRL + C stop listenning new connection
	signal(SIGINT, stop_process_listener);
	
	// stop listenning new connection AND kill sub processes
	signal(SIGTERM, stop_process_all);
	
	// don't keep exited children in the memory
	signal(SIGCHLD, SIG_IGN);
	
	const int listener_fd = server_init(port, MAX_CONNECTION_ALLOWED);
	
	if (listener_fd == -1) {
		fprintf(stderr, "Can't start the server on port %i\n", port);
		perror("Error");
		return EXIT_FAILURE;
	}
	
	printf("Listenning on %i on process ID [%i]\n", port, getpid());
	
	listen_client(listener_fd);
	
	close(listener_fd);
	
	printf("Stop server\n");
	
	return EXIT_SUCCESS;
}


void help_program() {
	char* help_text = ""
	"simple-ftp-daemon [PORT]"
	"\nstart a daemon (service) who wait and respond to the simple-ftp-client";
	"\n";
	printf("%s", help_text);
}


void listen_client(const int listener_fd) {
	int command_fd;
	struct sockaddr_in client_addr;
	socklen_t clilen = sizeof(clilen);
	
	pid_t pid;
	
	while (1) {
		// accept the connection of one client
		command_fd = accept(listener_fd, &client_addr, &clilen);
		if (command_fd == -1) {
			perror("Accept");
			exit(EXIT_FAILURE);
		}
		
		pid = fork();
		if (pid == 0) {
			// close listener socket in the client process
			close(listener_fd);
			
			// reset the subprocess behavior in the client process
			signal(SIGCHLD, SIG_DFL);
			
			// CTRL + C stop listenning new connection AND kill sub processes
			signal(SIGINT, stop_process_connection);
			
			client_process(command_fd, client_addr);
			
			// close listener socket in the client process
			close(command_fd);
			
			// exit the client process
			exit(EXIT_SUCCESS);
		}
		else if (pid > 0) {
			// close client socket in the main process
			close(command_fd);
		}
		else {
			perror("Client connection fork");
		}
	}
}


void client_process(const int command_fd, const struct sockaddr_in client_addr) {
	char address[255];
	int port;
	
	addr_info(client_addr, address, &port);
	
	printf("Connection from %s:%i on process ID [%i]\n", address, port, getpid());
	
	if (login(command_fd)) {
		return;
	}
	
	while (!do_request(command_fd, address, port));
	
	printf("Diconnection of %s:%i on process ID [%i]\n", address, port, getpid());
}


int login(const int command_fd) {
	char username[BUFFER_SIZE] = "";
	char password[BUFFER_SIZE] = "";
	
	int step = 0;
	if (++step && !socket_read_and_check(command_fd, "BONJ")
	 && ++step && !socket_write(command_fd, "WHO")
	 && ++step && read(command_fd, username, BUFFER_SIZE) > 0
	 && ++step && !socket_write(command_fd, "PASSWD")
	 && ++step && read(command_fd, password, BUFFER_SIZE) > 0
	 && ++step && !is_valid_login(username, password)
	 && ++step && !socket_write(command_fd, "WELC")) {
		fprintf(stderr, "Logged in “%s”!\n", username);
		return EXIT_SUCCESS;
	}
	
	fprintf(stderr, "Not logged in “%s”:%i [step %i]!\n", username, strlen(password), step);
	socket_write(command_fd, "BYE");
	
	return EXIT_FAILURE;
}


int is_valid_login(const char* username, const char* password) {
	char path[255];
	strcpy(path, getenv("HOME"));
	strcat(path, "/.config/simple-ftp/daemon/login.txt");
	
	FILE* file = fopen(path, "r");
	
	if (!file) {
		fprintf(stderr, "Login file “%s”\n", path);
		perror("Can't open login file un read mode");
		return EXIT_FAILURE;
	}
	
	int len;
	for (
		char line[BUFFER_SIZE];
		fgets(line, BUFFER_SIZE, file) != NULL;
		fgets(line, BUFFER_SIZE, file) // don't care of password line
	) {
		len = strlen(line);
		// delete \n at the end of line
		line[len ? len - 1 : 0] = '\0';
		
		// check if username match
		if (len && !strcmp(username, line)) {
			// get password line
			if (fgets(line, BUFFER_SIZE, file) != NULL) {
				len = strlen(line);
				// delete \n at the end of line
				line[len ? len - 1 : 0] = '\0';
				
				// check if password match
				if (len && !strcmp(password, line)) {
					return EXIT_SUCCESS;
				}
			}
			
			// username should be unique in the file
			break;
		}
	}
	
	return EXIT_FAILURE;
}


int do_request(const int command_fd, const char* address, const int port) {
	char cmd[BUFFER_SIZE] = "";
	
	if (read(command_fd, cmd, BUFFER_SIZE)) {
		fprintf(stderr, "%s:%i Requested command: %s\n", address, port, cmd);
	}
	
	if (!strcmp(cmd, "rls") || !strcmp(cmd, "rpwd")) {
		char* sys_cmd = "";
		if (!strcmp(cmd, "rls")) {
			sys_cmd = "ls -alch";
		}
		else {
			sys_cmd = "pwd";
		}
		return exec_sys_cmd(command_fd, sys_cmd);
	}
	else if (startsWith(cmd, "rcd ")) {
		char* path = strstrip(cmd + 4);
		if (chdir(path)) {
			// fprintf(stderr, "Can't change dir to %s\n", path);
			// perror("Error");
			// return EXIT_FAILURE;
			return socket_write(command_fd, "NOCD");
		}
		else {
			return socket_write(command_fd, "CDOK");
		}
		// return EXIT_SUCCESS;
	}
	else if (startsWith(cmd, "upld ")) {
		return response_upload_command(command_fd, address, cmd);
	}
	else if (startsWith(cmd, "downl ")) {
		return response_download_command(command_fd, address, cmd);
	}
	
	return EXIT_FAILURE;
}


int response_upload_command(const int command_fd, const char* address, const char* cmd) {
	char* path = strstrip(cmd + 5);
	
	FILE* file = fopen(path, "w");
	if (!file && socket_write(command_fd, "FBDN")) {
		printf("%s “w”\n", path);
		return EXIT_FAILURE;
	}
	fclose(file);
	
	if (socket_write(command_fd, "RDY")) {
		return EXIT_FAILURE;
	}
	
	return receive_file(command_fd, address, path);
}


int response_download_command(const int command_fd, const char* address, const char* cmd) {
	char* path = strstrip(cmd + 5);
	
	FILE* file = fopen(path, "r");
	if (!file && socket_write(command_fd, "UNKNOW")) {
		printf("%s “r”\n", path);
		return EXIT_FAILURE;
	}
	fclose(file);
	
	if (socket_write(command_fd, "RDY")) {
		return EXIT_FAILURE;
	}
	
	return send_file(command_fd, address, path);
}
