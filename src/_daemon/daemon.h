#ifndef DAEMON_H
	#define DAEMON_H
	
	#include "../core/core.h"
	
	#define MAX_CONNECTION_ALLOWED 2147483647
	
	void listen_client(const int listener_fd);
	void client_process(const int command_fd, const struct sockaddr_in client_addr);
	int login(const int command_fd);
	int is_valid_login(const char* username, const char* password);
	int do_request(const int command_fd, const char* address, const int port);
	int response_upload_command(const int command_fd, const char* address, const char* cmd);
	int response_download_command(const int command_fd, const char* address, const char* cmd);
#endif
