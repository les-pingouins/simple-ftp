#ifndef CLIENT_H
	#define CLIENT_H
	
	#include <sys/select.h>
	
	#include "../core/core.h"
	
	#define max(a,b) ((a) > (b) ? a : b)
	
	void help_cmd();
	void check_server_timeout(int sig);
	int login(const int command_fd, const char* username, const char* password);
	int login_interractive(const int command_fd);
	void listen_user_command_and_server(const int command_fd, const char* address);
	int user_command_handler(const int command_fd, const char* address);
	int read_server_response(const int command_fd);
	int request_transfert_command(const int command_fd, const char* address, const char* cmd, int(*transfert_function)(const int command_fd, const char* address, const char* cmd));
	int request_upload_command(const int command_fd, const char* address, const char* cmd);
	int request_download_command(const int command_fd, const char* address, const char* cmd);
#endif
