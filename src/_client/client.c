#include "client.h"

int main(int argc, char const *argv[]) {
	char const* address = "127.0.0.1";
	int port = COMMAND_PORT;
	
	if (argc >= 2) {
		if (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help")) {
			help_program();
			return EXIT_SUCCESS;
		}
		
		address = argv[1];
	}
	
	if (argc >= 3) {
		port = atoi(argv[2]);
	}
	
	// CTRL + C close connection
	signal(SIGINT, stop_process_listener);
	
	// close connection AND kill sub processes
	signal(SIGTERM, stop_process_all);
	
	// handle timeout
	signal(SIGALRM, check_server_timeout);
	
	const int command_fd = connect_to_server(address, port);
	
	if (!command_fd) {
		return EXIT_FAILURE;
	}
	
	if (login_interractive(command_fd)) {
		return EXIT_FAILURE;
	}
	
	listen_user_command_and_server(command_fd, address);
	
	close(command_fd);
	
	fprintf(stderr, "normal exit\n");
	
	return EXIT_SUCCESS;
}


void help_program() {
	char* help_text = ""
	"simple-ftp-client [HOST] [PORT]"
	"\nopen connection to the HOST:PORT"
	"\nask login and password"
	"\nthen wait for user command"
	"\n";
	printf("%s", help_text);
}


void help_cmd() {
	char* help_text = ""
	"List of available commands:"
	"\n"
	"\nLOCAL:"
	"\n    help        show this message"
	"\n    ls          show the list files"
	"\n    pwd         show the current working directory"
	"\n    cd DIR      change current working directory"
	"\n    rm FILE     delete a file"
	"\n    downl FILE  download a file from the remote"
	"\n    exit        disconnect and exit"
	"\nREMOTE:"
	"\n    rls         show the list files remotly"
	"\n    rpwd        show the current working directory remotly"
	"\n    rcd DIR     change current working directory remotly"
	"\n    upld FILE   upload a file to the remote"
	"\n";
	printf("%s", help_text);
}


void check_server_timeout(int sig) {
	perror("Timeout");
	fprintf(stderr, "Timeout: Server connection lost\n");
	exit(EXIT_FAILURE);
}


int login(const int command_fd, const char* username, const char* password) {
	int step = 0;
	if (++step && !socket_write(command_fd, "BONJ")
	 && ++step && !socket_read_and_check(command_fd, "WHO")
	 && ++step && !socket_write(command_fd, username)
	 && ++step && !socket_read_and_check(command_fd, "PASSWD")
	 && ++step && !socket_write(command_fd, password)
	 && ++step && !socket_read_and_check(command_fd, "WELC")) {
		fprintf(stderr, "Logged in “%s”!\n", username);
		return EXIT_SUCCESS;
	}
	
	fprintf(stderr, "Not logged in “%s”:%i [step %i]!\n", username, strlen(password), step);
	
	return EXIT_FAILURE;
}


int login_interractive(const int command_fd) {
	char username[BUFFER_SIZE] = "";
	char password[BUFFER_SIZE] = "";
	
	printf("Enter your username: ");
	fgets(username, BUFFER_SIZE, stdin);
	
	printf("Enter your password: ");
	fgets(password, BUFFER_SIZE, stdin);
	
	if (username == NULL || strlen(username) == 0 || !strcmp(username, "\n")
	 || password == NULL || strlen(password) == 0 || !strcmp(password, "\n")) {
		printf("Username or password is empty");
		return EXIT_FAILURE;
	}
	
	username[strlen(username) - 1] = '\0';
	password[strlen(password) - 1] = '\0';
	
	return login(command_fd, username, password);
}


void listen_user_command_and_server(const int command_fd, const char* address) {
	int fd_max_plus1 = max(STDIN_FILENO, command_fd) + 1;
	fd_set rdfs;
	FD_ZERO(&rdfs);
	FD_SET(STDIN_FILENO, &rdfs);
	FD_SET(command_fd, &rdfs);
	fd_set rdfs_initial = rdfs;
	int is_stop_writed = 0;
	struct timeval* p_timeout = NULL;
	struct timeval timeout;
	timeout.tv_sec = 20;
	timeout.tv_usec = 0;
	
	while (1) {
		if (select(fd_max_plus1, &rdfs, NULL, NULL, p_timeout) <= 0) {
			perror("Stream timeout");
			exit(EXIT_FAILURE);
		}
		
		if (FD_ISSET(command_fd, &rdfs)) {
			if (read_server_response(command_fd)) {
				fprintf(stderr, "End connection\n");
				break;
			}
			
			if (is_stop_writed) {
				break;
			}
		}
		
		if (!is_stop_writed && FD_ISSET(STDIN_FILENO, &rdfs)) {
			if (user_command_handler(command_fd, address)) {
				shutdown(command_fd, SHUT_WR);
				FD_CLR(STDIN_FILENO, &rdfs_initial);
				is_stop_writed = 1;
				p_timeout = &timeout;
			}
		}
		
		rdfs = rdfs_initial;
	}
}


int user_command_handler(const int command_fd, const char* address) {
	char cmd_read[BUFFER_SIZE] = "";
	ssize_t readed = -1;
	
	alarm(TIMEOUT_DELAY);
	if ((readed = read(STDIN_FILENO, cmd_read, BUFFER_SIZE)) == 0) {
		perror("Input read");
		return EXIT_FAILURE;
	}
	alarm(0);
	
	cmd_read[readed] = '\0';
	char* cmd = strstrip(cmd_read);
	
	if (!strcmp("exit", cmd)) {
		return EXIT_FAILURE;
	}
	
	if (!strcmp(cmd, "ls")) {
		return exec_sys_cmd(STDOUT_FILENO, "ls -alch");
	}
	else if (!strcmp(cmd, "pwd") || startsWith(cmd, "rm ")) {
		return exec_sys_cmd(STDOUT_FILENO, cmd);
	}
	else if (startsWith(cmd, "cd ")) {
		char* path = strstrip(cmd + 3);
		if (chdir(path)) {
			fprintf(stderr, "Can't change directory to %s\n", path);
			perror("Error");
			return EXIT_FAILURE;
		}
	}
	else if ('r' == cmd[0]) {
		alarm(TIMEOUT_DELAY);
		if (socket_write(command_fd, cmd)) {
			exit(EXIT_FAILURE);
		}
		
		if (startsWith(cmd, "rcd ")) {
			char socket_data[BUFFER_SIZE] = "";
			int read_length = read(command_fd, socket_data, BUFFER_SIZE);
			
			if (read_length == 0 || strcmp(socket_data, "CDOK")) {
				char* path = strstrip(cmd + 4);
				fprintf(stderr, "Can't change remote directory to %s\n", path);
				// return EXIT_FAILURE;
			}
		}
		alarm(0);
	}
	else if (startsWith(cmd, "upld ")) {
		return request_upload_command(command_fd, address, cmd);
	}
	else if (startsWith(cmd, "downl ")) {
		return request_download_command(command_fd, address, cmd);
	}
	else {
		help_cmd();
	}
	
	return EXIT_SUCCESS;
}


int read_server_response(const int command_fd) {
	char data[BUFFER_SIZE] = "";
	
	alarm(TIMEOUT_DELAY);
	int readed = -1;
	if ((readed = read(command_fd, data, BUFFER_SIZE)) <= 0) {
		perror("Connection lost to server");
		exit(EXIT_FAILURE);
	}
	data[readed] = '\0';
	alarm(0);
	
	printf("%s", data);
	fflush(stdout);
	
	return EXIT_SUCCESS;
}


int request_transfert_command(const int command_fd, const char* address, const char* cmd, int(*transfert_function)(const int command_fd, const char* address, const char* cmd)) {
	if (socket_write(command_fd, cmd)) {
		return EXIT_FAILURE;
	}
	
	alarm(TIMEOUT_DELAY);
	if (socket_read_and_check(command_fd, "RDY")) {
		return EXIT_FAILURE;
	}
	alarm(0);
	
	char* path = strstrip(cmd + 5);
	return transfert_function(command_fd, address, path);
}


int request_upload_command(const int command_fd, const char* address, const char* cmd) {
	return request_transfert_command(command_fd, address, cmd, send_file);
}


int request_download_command(const int command_fd, const char* address, const char* cmd) {
	return request_transfert_command(command_fd, address, cmd, receive_file);
}
