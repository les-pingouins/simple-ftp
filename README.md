# simple-ftp

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [simple-ftp](#simple-ftp)
	- [Install](#install)
	- [Usage](#usage)
	- [Documentation](#documentation)
	- [Licence](#licence)

<!-- /TOC -->

## Install

```sh
make compile # compile the programes
make install # install it to /usr/local/bin
```


## Usage

```sh
make compile # compile the programmes
# or
make run TARGET=daemon &>/dev/null # compile & run the daemon
make run TARGET=client # compile & run the client
```


## Documentation

The documentation must be genererated with [PlantUML](http://plantuml.com/download.html)
```sh
sudo apt-get install plantuml
```

```sh
make documentation # regenerate the documentation's files
```

![Client Process & Threads](./doc/client_process.svg)
![Daemon Process, Subprocesses & Threads](./doc/daemon_logic_final.svg)
![Daemon Logic](./doc/daemon_logic.svg)


## Licence

[Licence](./LICENCE.md)
